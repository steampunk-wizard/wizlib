from wizlib.ui.shell.line_editor import ShellLineEditor


if __name__ == '__main__':
    print(f"{ShellLineEditor(['glim','glam','glom'], 'glorious').edit()}")
