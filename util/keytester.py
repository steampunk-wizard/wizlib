from readchar import readkey

while True:
    key = readkey()
    print(" ".join(hex(ord(c)) for c in key))
