.PHONY: init dependencies test style build docs

PROJECT_NAME := $(shell basename "$(CURDIR)")

all: style test

init:
	rm -rf .venv
	python3.11 -m venv .venv
	.venv/bin/pip install --upgrade pip poetry


# Install dependencies
dependencies:
	.venv/bin/poetry install --no-root

test:
	.venv/bin/python -m coverage run --source=$(PROJECT_NAME) -m unittest -v 2>&1
	.venv/bin/python -m coverage report -m --fail-under 95

style:
	.venv/bin/autopep8 -ir .
	.venv/bin/pycodestyle $(PROJECT_NAME)

build:
	rm -rf dist
	.venv/bin/poetry build

docs:
	.venv/bin/pelican -lr docs -o public/html -s docs.py -p 9500
