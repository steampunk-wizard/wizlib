# ConfigHandler

Enables easy configuration across multiple approaches, providing the user
options for how to provide configurational information.

Commands can request values from the configuration using an expression of the form:

```python
val = self.app.config.get('myapp-host')
```

The argument is a hyphen-separated set of words that indicate a path through a
hierachy of possible values. 

Typically, the first word in the argument is the name of the application.

The hierarchy can have multiple levels.

To retrieve the configuration value, the handler will first look in a cache to see whether the value has already been retrieved, and if so, return it.

Failing the cache retrieval, the handler will  look for an environment variable by converting the hyphens to underscores and the words to all caps (for example, `MYAPP-HOST`)

If it finds no environment variable, the handler will look for a yaml configuration file, in one of the following ways:

- First, if the original WizLib app call included a `--config` / `-c` option (before the subcommand) then that option's value is used as the path to the config file
- Then with a path in the `MYAPP_CONFIG` environment variable - note all caps for the application name
- Then look in the local working directory for `.myapp.yml`
- Then look for `~/.myapp.yml` in the user's home directory

Config files are in YAML, and look something like this:

```yaml
myapp:
  host: api.example.com
```

The words from the argument to `config.get()` convert to levels in the YAML.

The configuration file can call os commands using `$(xxxxx)` syntax, but only in the YAML case. Doing so allows secrets to stay in a password manager until they are needed by the application.

```yaml
myapp:
  token: $(op read "op://Private/example/api-key")
```