# Command

Commands live in the `command/` directory and inherit from a single base command, which itself inherits from WizCommand

The basic framework of a command is shown below.

```python

class CreateCommand(MyAppCommand):

    # Subcommand users will type
    name = 'create'


    # Add arguments required by this subcommand - uses ArgParse.
    @classmethod
    def add_args(cls, parser: WizParser):
        parser.add_argument('value', nargs='?')


    # A method to handle actual values. Note they have been converted
    # to attributes of the command itself by this point. Use
    # self.provided to check if a user provided the argument.
    def handle_vals(self):
        super().handle_vals()
        if not self.provided('value'):
            self.value = input('Value to create: ')

    # Actually perform the command
    @QueueCommand.wrap
    def execute(self):
        MyModelClass.create(value)
        self.status = 'Created'
```
