# ClassFamily

A class family is a set of class definitions that use single inheritance
(each subclass inherits from only one parent) and often multiple inheritance
(subclasses can inherit from subclasses). So it's a hierarchy of classes,
with one super-parent (termed the "atriarch") at the top.

We offer a way for members of the family to declare themselves simply by
living in the right package location. Then those classes can be instantiated
using keys or names, without having to be specifically called. The members
act independently of each other.

What we get, after importing everything and loading it all, is essentially a
little database of classes, where class-level properties become keys for
looking up member classes. So, for example, we can have a family of commands,
and use a command string to look up the right command.

Ultimately, the atriarch of the family -- the class at the top of the
hierarchy -- holds the database, actually a list, in the property called
"family". So that class can be queried to find appropriate family member
classes or instances thereof.

This utility provides functions for importing family members, loading the
"families" property of the super-parent, and querying the family.

In the process of loading and querying the class family, we need to *avoid*
inheritance of attributes. There might be abstract intermediary classes that
don't want to play. So we use `__dict__` to ensure we're only seeing the
atttributes that are defined on that specific class.