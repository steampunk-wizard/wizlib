# StreamHandler

When enabled, simplifies handling of input via stdin for non-tty inputs such as
pipes. Optionally, users can specify a file other than stdin using the
`--stream` option.

It's not really a stream. Everything gets read at once when the command starts. The value is available to commands using the following expression:

```python
self.app.stream.text
```
