from io import StringIO
import os
import sys
from wizlib.test_case import WizLibTestCase
from unittest.mock import patch
from test.dummy import DummyApp

from wizlib.ui import UI
from wizlib.ui.shell import ESC
from wizlib.ui.shell_ui import ShellUI


class TestShellUI(WizLibTestCase):

    def test_exists(self):
        u = UI.family_member('name', 'shell')()
        self.assertIsInstance(u, ShellUI)

    def test_output(self):
        u = UI.family_member('name', 'shell')()
        with self.patcherr() as e:
            u.send('j')
        e.seek(0)
        self.assertEqual(e.read(), ESC + '[36mj' + ESC + '[0m\n')

    def test_get_option(self):
        with \
                self.patchout() as o, \
                self.patcherr(), \
                self.patch_ttyin('a'):
            DummyApp.start('eat')
        o.seek(0)
        self.assertEqual('Yummy apples', o.read())

    def test_get_text_from_user(self):
        with \
                self.patchout() as o, \
                self.patcherr(), \
                self.patch_ttyin("I love cheese\n"):
            DummyApp.start('talk')
        o.seek(0)
        self.assertEqual('You said "I love cheese"', o.read())

    def test_default_option_key(self):
        with \
                self.patchout() as o, \
                self.patch_ttyin("\n"):
            DummyApp.start('thank')
        o.seek(0)
        self.assertEqual('You said yes', o.read())
