from argparse import ArgumentParser

from . import DummyCommand


class DanceCommand(DummyCommand):
    """Simple test of stream handler"""

    name = 'dance'

    @classmethod
    def add_args(self, parser: ArgumentParser):
        parser.add_argument('--style')

    def handle_vals(self):
        super().handle_vals()
        if not self.provided('style'):
            self.style = self.app.stream.text

    @DummyCommand.wrap
    def execute(self):
        return f"Dancing {self.style}"
