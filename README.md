
# WizLib

Build configurable CLI tools easily in Python (a framework)

<a style="font-weight: bold; font-size: 1.2em;" href="https://wizlib.steamwiz.io">Documentation on SteamWiz.io</a>

<a style="font-weight: 300; font-size: 0.8em;" href="https://www.flaticon.com/free-icons/wizard" title="wizard icons">Wizard icon by Freepik-Flaticon</a>
