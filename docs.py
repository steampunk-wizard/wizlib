# Configuration file for Pelican-based documentations

AUTHOR = 'Francis Potter'
SITENAME = 'WizLib framework for command-line devops and personal productivity tools'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Vancouver'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

LINKS = (('Home', '/'),
         ('Reference', '/pages/reference'),
         ('Sample app', 'https://gitlab.com/wizlib/sample'),
         ('Library Source', 'https://gitlab.com/wizlib/wizlib'),
         ('PyPI entry', 'https://pypi.org/project/wizlib/'),
         ('Related projects', 'https://gitlab.com/wizlib')
         )

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

THEME = "theme"
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
PLUGINS = ['plugins.i18n_subsites']
# MENUITEMS = [("Documentation","/documentation")]
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False
# CATEGORY_URL = "category/{slug}.html"
USE_FOLDER_AS_CATEGORY = True
STATIC_PATHS = ['media']
