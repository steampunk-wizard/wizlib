Title: RLInput
Slug: rlinput

_Note: Python 3.12 intriduced a condition under which GNU readline seems to
function differently. Use Python 3.11 with the RLInput capability of WizLib._

Python supports the GNU readline approach, which enables tab completion, key
mappings, and history with the `input()` function. But the documentation is
cryptic, and the implementation differs between Linux and MacOS. RLInput makes
it easy.

```python
from wizlib.rlinput import rlinput
```

It's just a function, with up to three parameters:

- `intro:str=""` - The intro or prompt, same as in the `input()` function.
- `default:str=""` - If provided, the text will be inserted into the buffer at
  the start, with the cursor at the end of the buffer. So that becomes the
  default, that must be overridden by the user if they want different input.
- `options:list=[]` - A list of options for tab completion. This assumes the
  options are choices for the entire entry; it's not context-dependent within
  the buffer.

Emacs keys are enabled by default; I'm able to use the arrow keys on my Mac so
you should too. So to wipe out the default value and type or tab something
new, press ctrl-a then ctrl-k.