from io import StringIO
from wizlib.test_case import WizLibTestCase
from unittest.mock import patch
from wizlib.config_handler import ConfigHandler

from wizlib.stream_handler import StreamHandler
from test.dummy import DummyApp
from wizlib.ui.shell_ui import ShellUI


class TestHandlers(WizLibTestCase):

    # def test_help(self):
    #     with self.patchout() as o:
    #         DummyApp.start('--help', debug=True)
    #     o.seek(0)
    #     self.assertIn('--config CONFIG', o.read())

    def test_ui_handler_shell(self):
        app = DummyApp.initialize(ui='shell')
        self.assertIsInstance(app.ui, ShellUI)

    # def test_ui_choices_in_help(self):
    #     with self.patchout() as o:
    #         DummyApp.start('--help', debug=True)
    #     o.seek(0)
    #     self.assertIn('--ui {', o.read())
